import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

import { PagesRoutingModule } from './pages/pages-routing.module'
import { AppComponent } from './app.component'
import { MenuModule } from './shared-modules/menu/menu.module'
import { HomeModule } from './pages/home/home.module'
import { TitleModule } from './pages/title/title.module'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { AuthInterceptorService } from './core-services/auth-interceptor/auth-interceptor.service'
import { environment } from '../environments/environment'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PagesRoutingModule,
    MenuModule,
    HomeModule,
    TitleModule,
    HttpClientModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
