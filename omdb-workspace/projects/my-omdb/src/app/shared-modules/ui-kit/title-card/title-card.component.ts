import { ChangeDetectionStrategy, Component, Input, OnChanges } from '@angular/core'
import { TitleCard } from './title-card.interface'

@Component({
  selector: 'mdb-title-card',
  templateUrl: './title-card.component.html',
  styleUrls: ['./title-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TitleCardComponent {
  @Input() card: TitleCard

  constructor() { }


}
