export interface TitleCard {
  Title: string
  Poster: string
  Type: string
  Year: number
  imdbID: string
}
