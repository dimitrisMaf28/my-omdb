import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { TitleCardComponent } from './title-card.component'
import { BrowserModule } from '@angular/platform-browser'
import { RouterModule } from '@angular/router'



@NgModule({
  declarations: [
    TitleCardComponent
  ],
  exports: [
    TitleCardComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
  ]
})
export class TitleCardModule { }
