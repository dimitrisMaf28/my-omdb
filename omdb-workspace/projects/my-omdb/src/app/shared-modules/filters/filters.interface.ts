export interface Filters {
  search?: string | undefined,
  type?: string,
  page?: number
}
