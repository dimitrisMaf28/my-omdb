import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core'
import { Filters } from './filters.interface'

@Component({
  selector: 'mdb-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FiltersComponent {
  @Input() filters: Filters = {
    search: undefined,
    type: ''
  }
  @Output() filtersChange = new EventEmitter<Filters>()
  types = [
    {name: 'All title types', value: undefined },
    {name: 'Movie', value: 'movie'},
    {name: 'Series', value: 'series'},
    {name: 'Episode', value: 'episode'}
  ]

  constructor() { }

  setType(type: string, filters: Filters) {
    this.submit({...filters, type})
  }

  submit(filters: Filters) {
    this.filtersChange.emit(filters)
  }

}
