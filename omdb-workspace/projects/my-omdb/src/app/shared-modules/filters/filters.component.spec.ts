import { ComponentFixture, TestBed } from '@angular/core/testing'

import { FiltersComponent } from './filters.component'

describe('FiltersComponent', () => {
  let component: FiltersComponent
  let fixture: ComponentFixture<FiltersComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiltersComponent ]
    })
    .compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltersComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should set type & emit', () => {
    const spy = spyOn(component.filtersChange, 'emit')
    component.filters = {
      search: 'some search',
      type: 'movie',
    }
    component.submit(component.filters)
    expect(spy.calls.mostRecent().args[0]).toEqual(component.filters)

    component.setType('episode', component.filters)
    expect(spy.calls.mostRecent().args[0]).toEqual({
      ...component.filters,
      type: 'episode'
    })
  })
})
