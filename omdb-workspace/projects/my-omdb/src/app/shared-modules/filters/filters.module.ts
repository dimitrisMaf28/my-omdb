import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FiltersComponent } from './filters.component'
import { FormsModule } from '@angular/forms'



@NgModule({
  declarations: [
    FiltersComponent
  ],
  exports: [
    FiltersComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
  ]
})
export class FiltersModule { }
