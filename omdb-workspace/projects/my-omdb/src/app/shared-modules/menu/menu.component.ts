import { Component, OnInit } from '@angular/core'
import { faMoon, faSun } from '@fortawesome/free-regular-svg-icons'
import { Theme } from '../../enums/theme.enum'

@Component({
  selector: 'mdb-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  theme: Theme

  icons = {faMoon, faSun}

  constructor() { }

  ngOnInit() {
    this.theme = localStorage.getItem('data-theme') === Theme.light ? Theme.light : Theme.dark
    document.documentElement.setAttribute('data-theme', this.theme)
  }

  toggleTheme() {
    this.theme = this.theme === Theme.light ? Theme.dark : Theme.light
    localStorage.setItem('data-theme', this.theme)
    document.documentElement.setAttribute('data-theme', this.theme)
  }

}
