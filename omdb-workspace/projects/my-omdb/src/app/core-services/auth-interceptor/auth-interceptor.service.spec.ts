import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { TestBed, fakeAsync, tick } from '@angular/core/testing'
import { AuthInterceptorService } from './auth-interceptor.service'
import { environment } from 'projects/my-omdb/src/environments/environment'


describe('AuthInterceptorService', () => {
  let httpTestingController: HttpTestingController
  let httpClient: HttpClient

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [
        AuthInterceptorService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptorService,
          multi: true,
        },
      ]
    })
    httpTestingController = TestBed.inject(HttpTestingController)
    httpClient = TestBed.inject(HttpClient)
  })

  it('should be created', () => {
    const service: AuthInterceptorService = TestBed.inject(AuthInterceptorService)
    expect(service).toBeTruthy()
  })

  it('should be intercept with authorization in headers', fakeAsync(() => {
    const url = 'myApi/get'
    httpClient.get<any>(url, {}).subscribe(data => {})
    tick(12)
    // const httpRequest = httpTestingController.expectOne(url)
    // expect(httpRequest.request.params.get('apikey')).toEqual(environment.api_key)
  }))

})
