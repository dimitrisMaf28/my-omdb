import { Injectable } from '@angular/core'
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http'
import { Observable } from 'rxjs'
import { finalize } from 'rxjs/operators'
import { environment } from 'projects/my-omdb/src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable <HttpEvent<any>> {
    this.startLoader()
    const req_with_key = this.cloneRequest(req, environment.api_key)
    return next.handle(req_with_key).pipe(
      finalize(() => this.closeLoader())
    )
  }

  private cloneRequest(req: HttpRequest<any>, apikey: string): HttpRequest<any> {
    return req.clone({
      setHeaders: {},
      params: req.params.set('apikey', apikey)
    })
  }

  private startLoader() {
    // this.store.dispatch(loaderAppendToQueue())
  }

  private closeLoader() {
    // this.store.dispatch(loaderRemoveFromQueue())
  }
}
