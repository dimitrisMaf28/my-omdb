import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { HomeComponent } from './home/home.component'
import { ResolverService as HomeResolver } from './home/resolver/resolver.service'
import { ResolverService as TitleResolver } from './title/resolver/resolver.service'
import { TitleComponent } from './title/title.component'

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    resolve: {
      data: HomeResolver
    },
    runGuardsAndResolvers: 'paramsOrQueryParamsChange'
  },
  {
    path: 'title/:id',
    component: TitleComponent,
    resolve: {
      data: TitleResolver
    },
    runGuardsAndResolvers: 'paramsOrQueryParamsChange'
  },
  {path: '**', redirectTo: '', pathMatch: 'full'},

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
