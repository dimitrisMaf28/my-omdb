import { NO_ERRORS_SCHEMA } from '@angular/core'
import { ComponentFixture, TestBed } from '@angular/core/testing'
import { Title } from '@angular/platform-browser'
import { ActivatedRoute, Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { cold } from 'jasmine-marbles'
import { Filters } from '../../shared-modules/filters/filters.interface'
import { IReturn } from './resolver/resolver.service'
import { TitleComponent } from './title.component'

describe('TitleComponent', () => {
  let component: TitleComponent
  let fixture: ComponentFixture<TitleComponent>
  let route: ActivatedRoute
  let router: Router
  const titleSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('Title', ['setTitle'])

  const data: IReturn = {
    title: {
      Title: 'Some title'
    },
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TitleComponent ],
      imports: [
        RouterTestingModule.withRoutes([
          {path: '**', component: TitleComponent}
        ]),
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: Title, useValue: titleSpy},
      ]
    })
    .compileComponents()
  })

  beforeEach(() => {
    route = TestBed.inject(ActivatedRoute)
    router = TestBed.inject(Router)
    route.data = cold('(a)', {a: {data}})

    fixture = TestBed.createComponent(TitleComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
    expect(component.data$).toBeObservable(cold('(a)', {a: data}))
    expect(titleSpy.setTitle.calls.mostRecent().args[0]).toEqual(`${data.title.Title} | OMDb`)
  })
})
