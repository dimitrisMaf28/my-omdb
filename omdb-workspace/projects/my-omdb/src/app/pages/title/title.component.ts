import { Component, OnInit } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'
import { map, tap } from 'rxjs/operators'
import { IReturn } from './resolver/resolver.service'

@Component({
  selector: 'mdb-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent {
  data$: Observable<IReturn> = this.route.data.pipe(
    map(res => res.data),
    tap(res =>
      this.title.setTitle(`${res?.title?.Title} | OMDb`)
    )
  )

  constructor(
    private route: ActivatedRoute,
    private title: Title,
  ) { }

}
