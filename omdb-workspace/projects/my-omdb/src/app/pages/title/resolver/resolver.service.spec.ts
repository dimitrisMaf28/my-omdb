import { TestBed } from '@angular/core/testing'
import { ActivatedRouteSnapshot} from '@angular/router'
import { HttpWrapperService, RequestResponse } from 'core-lib'
import { cold } from 'jasmine-marbles'
import { ResolverService } from './resolver.service'

describe('ResolverService', () => {
  let service: ResolverService
  const httpSpy: jasmine.SpyObj<HttpWrapperService> = jasmine.createSpyObj('HttpWrapperService', ['get'])

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: HttpWrapperService, useValue: httpSpy}
      ]
    })
    service = TestBed.inject(ResolverService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('should resolve', () => {
    const res: RequestResponse = {
      body: {},
      error: 'some error'
    }
    httpSpy.get.and.returnValue(cold('(a|)', {a: res}))
    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot()
    const mock_param = 'route params'
    spyOn(route.queryParamMap, 'get').and.returnValue(mock_param)

    expect(service.resolve(route)).toBeObservable(cold('(a|)', {a: {
      title: {},
      error: res.error,
    }}))
  })
})
