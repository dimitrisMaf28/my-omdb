import { Injectable } from '@angular/core'
import { ActivatedRouteSnapshot, Resolve } from '@angular/router'
import { HttpWrapperService, RequestParams } from 'core-lib'
import { Observable } from 'rxjs'
import { first, map, shareReplay } from 'rxjs/operators'
import { API } from '../../../configs/api'
import { TitleCard } from '../../../shared-modules/ui-kit/title-card/title-card.interface'

export interface IReturn {
  title: any
  error?: string | undefined
}

@Injectable({
  providedIn: 'root'
})
export class ResolverService implements Resolve<IReturn> {

  constructor(
    private http: HttpWrapperService,
  ) { }

  resolve( route: ActivatedRouteSnapshot ): Observable<IReturn> {
    const params: RequestParams = this.params(route)
    return this.title(params)
  }

  private title(params: RequestParams): Observable<IReturn> {
    return this.http.get(params).pipe(
      map(res => ({
        title: res.body,
        error: res.error
      })),
      first(),
      shareReplay()
    )
  }

  private params(route: ActivatedRouteSnapshot): RequestParams {
    const i = route.paramMap.get('id') || ''
    return {
      url: API.titles,
      params: {i}
    }
  }
}
