import { Component, OnInit } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { ActivatedRoute, Router } from '@angular/router'
import { Observable } from 'rxjs'
import { map, shareReplay } from 'rxjs/operators'
import { Filters } from '../../shared-modules/filters/filters.interface'
import { IReturn } from './resolver/resolver.service'

@Component({
  selector: 'mdb-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  data$: Observable<IReturn> = this.route.data.pipe(
    map(res => res.data),
    shareReplay()
  )

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private title: Title,
  ) { }

  ngOnInit() {
    this.title.setTitle('Titles | OMDb')
  }

  setFilters(filters: Filters) {
    this.navigate({...filters, page: 1})
  }

  setPage(filters: Filters, page: number) {
    this.navigate({...filters, page})
  }

  private navigate(filters: Filters) {
    this.router.navigate(['/'], {queryParams: {
      ...(filters.search ? {s: filters.search} : {}),
      ...(filters.type ? {type: filters.type} : {}),
      page: filters.page
    }})
  }
}
