import { HttpClient, HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { ActivatedRouteSnapshot, Resolve } from '@angular/router'
import { HttpWrapperService, Pagination, RequestParams } from 'core-lib'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { API } from '../../../configs/api'
import { Filters } from '../../../shared-modules/filters/filters.interface'
import { TitleCard } from '../../../shared-modules/ui-kit/title-card/title-card.interface'

export interface IReturn {
  pagination: Pagination
  titles: TitleCard[]
  error?: string | undefined
  filters: Filters
}

@Injectable({
  providedIn: 'root'
})
export class ResolverService implements Resolve<IReturn> {

  constructor(
    private http: HttpWrapperService,
  ) { }

  resolve( route: ActivatedRouteSnapshot): Observable<IReturn> {
    const params: RequestParams = this.params(route)
    return this.titles(params)
  }

  private titles(params: RequestParams): Observable<IReturn> {
    return this.http.get(params).pipe(
      map(res => ({
        pagination: {
          active_page: params.params?.page || 1,
          total_items: res.body?.totalResults || 1,
          per_page: 10
        },
        titles: res.body?.Search,
        error: res.error,
        filters: {
          search: params.params?.s,
          type: params.params?.type,
          page: params.params?.page,
        }
      })),
    )
  }

  private params(route: ActivatedRouteSnapshot): RequestParams {
    return {
      url: API.titles,
      params: {
        s: route.queryParamMap.get('s') || 'office',
        // A default value has been added in order to avoid too many results/wrong id error from the API
        type: route.queryParamMap.get('type') || undefined,
        page: parseInt(route.queryParamMap.get('page') || '', 10) || 1
      }
    }
  }
}
