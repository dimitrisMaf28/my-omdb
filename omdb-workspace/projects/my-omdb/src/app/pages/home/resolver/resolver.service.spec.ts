import { TestBed } from '@angular/core/testing'
import { ActivatedRouteSnapshot} from '@angular/router'
import { HttpWrapperService, RequestResponse } from 'core-lib'
import { cold } from 'jasmine-marbles'
import { ResolverService } from './resolver.service'

describe('ResolverService', () => {
  let service: ResolverService
  const httpSpy: jasmine.SpyObj<HttpWrapperService> = jasmine.createSpyObj('HttpWrapperService', ['get'])

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: HttpWrapperService, useValue: httpSpy}
      ]
    })
    service = TestBed.inject(ResolverService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('should resolve', () => {
    const res: RequestResponse = {
      body: {
        Search: []
      },
    }
    httpSpy.get.and.returnValue(cold('(a|)', {a: res}))
    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot()
    const mock_param = 'route params'
    spyOn(route.queryParamMap, 'get').and.returnValue(mock_param)

    expect(service.resolve(route)).toBeObservable(cold('(a|)', {a: {
      pagination: {
        active_page: 1,
        total_items: 1,
        per_page: 10
      },
      titles: res.body.Search,
      error: res.error,
      filters: {
        search: mock_param,
        type: mock_param,
        page: 1,
      }
    }}))
  })
})
