import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HomeComponent } from './home.component'
import { TitleCardModule } from '../../shared-modules/ui-kit/title-card/title-card.module'
import { FiltersModule } from '../../shared-modules/filters/filters.module'
import { PaginationModule } from 'core-lib'



@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    TitleCardModule,
    FiltersModule,
    PaginationModule,
  ]
})
export class HomeModule { }
