import { NO_ERRORS_SCHEMA } from '@angular/core'
import { ComponentFixture, TestBed } from '@angular/core/testing'
import { Title } from '@angular/platform-browser'
import { ActivatedRoute, Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { cold } from 'jasmine-marbles'
import { Filters } from '../../shared-modules/filters/filters.interface'
import { HomeComponent } from './home.component'
import { IReturn } from './resolver/resolver.service'

describe('HomeComponent', () => {
  let component: HomeComponent
  let fixture: ComponentFixture<HomeComponent>
  let route: ActivatedRoute
  let router: Router
  const titleSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('Title', ['setTitle'])

  const data: IReturn = {
    filters: {
      search: 'some search',
      page: 1
    },
    pagination: {
      active_page: 1,
      per_page: 10,
      total_items: 10
    },
    titles: []
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [
        RouterTestingModule.withRoutes([
          {path: '**', component: HomeComponent}
        ]),
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: Title, useValue: titleSpy},
      ]
    })
    .compileComponents()
  })

  beforeEach(() => {
    route = TestBed.inject(ActivatedRoute)
    router = TestBed.inject(Router)
    route.data = cold('(a)', {a: {data}})

    fixture = TestBed.createComponent(HomeComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
    expect(component.data$).toBeObservable(cold('(a)', {a: data}))
    expect(titleSpy.setTitle.calls.mostRecent().args[0]).toEqual('Titles | OMDb')
  })

  it('should set filters', () => {
    const filters: Filters = {
      page: 4342,
      search: 'black',
      type: 'type'
    }
    const spy = spyOn(router, 'navigate')
    component.setFilters(filters)
    expect(spy.calls.mostRecent().args).toEqual([
      ['/'],
      {
        queryParams: {
          s: filters.search,
          type: filters.type,
          page: 1
        }
      }
    ])
  })

  it('should set clear filters', () => {
    const filters: Filters = {}
    const spy = spyOn(router, 'navigate')
    component.setFilters(filters)
    expect(spy.calls.mostRecent().args).toEqual([
      ['/'],
      {
        queryParams: {
          page: 1
        }
      }
    ])
  })

  it('should set page', () => {
    const filters: Filters = {
      page: 4342,
      search: 'black',
      type: 'type'
    }
    const spy = spyOn(router, 'navigate')
    component.setPage(filters, 2)
    expect(spy.calls.mostRecent().args).toEqual([
      ['/'],
      {
        queryParams: {
          s: filters.search,
          type: filters.type,
          page: 2
        }
      }
    ])
  })
})
