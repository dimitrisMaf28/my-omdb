import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core'
import { Pagination } from './pagination.interface'
import { get } from 'lodash'
import { faAngleLeft, faAngleRight} from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'lib-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent implements OnChanges {
  @Input() pagination: Pagination | null = {
    active_page: 1,
    total_items: 0,
    per_page: 0
  }
  @Input() maxPageTiles = 6
  @Input() class: string
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>()
  totalPages = 1
  pagesArray: number[] = []
  faAngleLeft = faAngleLeft
  faAngleRight = faAngleRight

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.pagination) {
      if (this.pagination && !!this.pagination.total_items) {
        this.totalPages = this.calculateTotal(this.pagination)
        this.pagesArray = this.calculatePages(this.pagination, this.maxPageTiles, this.totalPages)
      } else this.clearPagination()
    }
  }

  private clearPagination() {
    this.totalPages = 1
  }

  private calculateTotal(pagination: Pagination): number {
    return Math.ceil(pagination.total_items / pagination.per_page)
  }

  private calculatePages(pagination: Pagination, maxPageTiles: number, totalPages: number) {
    const max = maxPageTiles < totalPages ? maxPageTiles : totalPages - 1
    const step = Math.floor(max / 2)
    let base = 1
    if (pagination.active_page > step && pagination.active_page < totalPages - step && totalPages > max) {
      base = pagination.active_page - step
    } else if ( pagination.active_page >= totalPages - step ) {
      base = totalPages - max
    }
    return Array(max).fill(max).map((value, index) => index + base)
  }

  activatePage(page: number) {
    if (!this.pagination) return
    this.pagination.active_page = page
    this.pagesArray = this.calculatePages(this.pagination, this.maxPageTiles, this.totalPages)
    this.pageChange.emit(page)
  }

  nextPage() {
    if (!this.pagination) return
    if (this.pagination.active_page < this.totalPages) this.activatePage(this.pagination.active_page + 1)
  }

  previousPage() {
    if (!this.pagination) return
    if (this.pagination.active_page > 1) this.activatePage(this.pagination.active_page - 1)
  }

  previousIsAvailable(page: number): boolean {
    return page > 1
  }

  nextIsAvailable(page: number): boolean {
    return page < this.totalPages
  }

  first() {
    this.activatePage(1)
  }

  last() {
    this.activatePage(this.totalPages)
  }

  firstPageActive(): boolean {
    return this.pageIsActive(1)
  }

  lastPageActive(): boolean {
    return this.pageIsActive(this.totalPages)
  }

  pageIsActive(page: number): boolean {
    return get(this.pagination, 'active_page') === page
  }

  showFirstPage(): boolean {
    return this.pagesArray[0] > 1
  }

  dotsBeforeActive(): boolean {
    return this.pagesArray[0] > 2
  }

  dotsAfterActive(): boolean {
    return this.pagesArray[this.pagesArray.length - 1] < this.totalPages - 1
  }

}
