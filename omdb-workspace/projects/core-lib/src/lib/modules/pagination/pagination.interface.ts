export interface Pagination {
  active_page: number
  total_items: number
  per_page: number
}
