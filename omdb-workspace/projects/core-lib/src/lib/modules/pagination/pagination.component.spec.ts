import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing'

import { PaginationComponent } from './pagination.component'
import { SimpleChanges, SimpleChange } from '@angular/core'

describe('PaginationComponent', () => {
  let component: PaginationComponent
  let fixture: ComponentFixture<PaginationComponent>
  let emitSpy: any

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginationComponent ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent)
    component = fixture.componentInstance
    emitSpy = spyOn(component.pageChange, 'emit')
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should activate page', () => {
    component.activatePage(3)
    expect(emitSpy.calls.mostRecent().args[0]).toBe(3)
  })

  it('should activate first page', () => {
    component.first()
    expect(emitSpy.calls.mostRecent().args[0]).toBe(1)
  })

  it('should activate first page', () => {
    component.last()
    expect(emitSpy.calls.mostRecent().args[0]).toBe(1)
    component.totalPages = 354
    component.last()
    expect(emitSpy.calls.mostRecent().args[0]).toBe(354)
  })

  it('should check if previous page is available', () => {
    expect(component.previousIsAvailable(2)).toBeTruthy()
    expect(component.previousIsAvailable(4)).toBeTruthy()
    expect(component.previousIsAvailable(1)).toBeFalsy()
  })

  it('should check if next page is available', () => {
    expect(component.nextIsAvailable(1)).toBeFalsy()
    component.totalPages = 5
    expect(component.nextIsAvailable(1)).toBeTruthy()
    expect(component.nextIsAvailable(4)).toBeTruthy()
    expect(component.nextIsAvailable(123)).toBeFalsy()
    expect(component.nextIsAvailable(5)).toBeFalsy()
  })

  it('should next & previous page', () => {
    component.pagination = {
      active_page: 1,
      total_items: 12,
      per_page: 5
    }
    component.totalPages = 3
    expect(component.pagination.active_page).toBe(1)
    component.previousPage()
    expect(component.pagination.active_page).toBe(1)
    component.nextPage()
    expect(emitSpy.calls.mostRecent().args[0]).toBe(2)
    component.pagination.active_page = 3
    expect(emitSpy.calls.mostRecent().args[0]).toBe(2)
    component.pagination.active_page = 2
    component.previousPage()
    expect(emitSpy.calls.mostRecent().args[0]).toBe(1)
  })

  it('should calculate total on change', () => {
    component.pagination = {
      active_page: 1,
      total_items: 18,
      per_page: 5
    }
    let changesObj: SimpleChanges = {
      pagination: new SimpleChange(null, component.pagination, true)
    }
    component.ngOnChanges(changesObj)
    expect(component.totalPages).toBe(4)
    component.pagination = null
    changesObj = {
      pagination: new SimpleChange(null, component.pagination, true)
    }
    component.ngOnChanges(changesObj)
    expect(component.totalPages).toBe(1)
  })

  it('should add active class if page is active', () => {
    component.pagination = {
      active_page: 3,
      total_items: 18,
      per_page: 5
    }
    component.totalPages = 20
    expect(component.firstPageActive()).toBeFalsy()
    expect(component.lastPageActive()).toBeFalsy()
    expect(component.pageIsActive(3)).toBeTruthy()
    component.pagination.active_page = 1
    expect(component.firstPageActive()).toBeTruthy()
    expect(component.lastPageActive()).toBeFalsy()
    expect(component.pageIsActive(3)).toBeFalsy()
    component.pagination.active_page = 20
    expect(component.firstPageActive()).toBeFalsy()
    expect(component.lastPageActive()).toBeTruthy()
    expect(component.pageIsActive(3)).toBeFalsy()
  })

  it('should show / hide dots', () => {
    component.pagesArray = [1, 2, 3, 4]
    component.totalPages = 12
    expect(component.showFirstPage()).toBeFalsy()
    expect(component.dotsBeforeActive()).toBeFalsy()
    expect(component.dotsAfterActive()).toBeTruthy()

    component.pagesArray = [5, 6, 7, 8]
    component.totalPages = 12
    expect(component.showFirstPage()).toBeTruthy()
    expect(component.dotsBeforeActive()).toBeTruthy()
    expect(component.dotsAfterActive()).toBeTruthy()

    component.pagesArray = [9, 10, 11, 12]
    component.totalPages = 12
    expect(component.showFirstPage()).toBeTruthy()
    expect(component.dotsBeforeActive()).toBeTruthy()
    expect(component.dotsAfterActive()).toBeFalsy()
  })

  it('should calculate pages on change', () => {
    component.pagination = {
      active_page: 1,
      total_items: 48,
      per_page: 5
    }
    const changesObj: SimpleChanges = {
      pagination: new SimpleChange(null, component.pagination, true)
    }
    component.ngOnChanges(changesObj)
    expect(component.pagesArray).toEqual([1, 2, 3, 4, 5, 6])
    expect(component.totalPages).toBe(10)
    component.activatePage(6)
    expect(component.pagesArray).toEqual([3, 4, 5, 6, 7, 8])
  })

  it('should calculate pages on change, less total pages', () => {
    component.pagination = {
      active_page: 1,
      total_items: 15,
      per_page: 5
    }
    const changesObj: SimpleChanges = {
      pagination: new SimpleChange(null, component.pagination, true)
    }
    component.ngOnChanges(changesObj)
    expect(component.pagesArray).toEqual([1, 2])
    expect(component.totalPages).toBe(3)
    component.activatePage(3)
    expect(component.pagesArray).toEqual([1, 2])
  })

  it('should clear pagination if total is not valid', () => {
    component.pagination = {
      active_page: 1,
      total_items: 0,
      per_page: 5
    }
    const changesObj: SimpleChanges = {
      pagination: new SimpleChange(null, component.pagination, true)
    }
    component.ngOnChanges(changesObj)
    expect(component.pagesArray).toEqual([])
    expect(component.totalPages).toBe(1)
  })
})
