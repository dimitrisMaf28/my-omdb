import { QueryParams } from './query-params.interface'

export interface RequestParams {
  url: string
  id?: string
  params?: QueryParams
}
