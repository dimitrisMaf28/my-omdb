export interface QueryParams  {
  s?: string,
  i?: string,
  page?: number
  type?: string
}
