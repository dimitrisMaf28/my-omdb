import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import { RequestParams } from '../../models/request-params.interface'
import { QueryParams } from '../../models/query-params.interface'
import { RequestResponse } from '../../models/request-response.interface'
import { HttpClient, HttpParams } from '@angular/common/http'
import { get } from 'lodash'
import { catchError, first, map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class HttpWrapperService {

  constructor(
    private http: HttpClient
  ) { }

  get(params: RequestParams): Observable<RequestResponse> {
    const httpParams = this.httpParams(params.params)
    const url = params.id ? `${params.url}/${params.id}` : params.url
    return this.http.get(url, {params: httpParams}).pipe(
      first(),
      map((res: any) => ({
        body: res,
        error: get(res, 'Error')
      })),
      catchError( () => of({
        body: undefined,
        error: 'An error occured'
      }))
    )
  }

  private httpParams(params: QueryParams | undefined): HttpParams {
    let httpParams = new HttpParams()
    if (!params) return httpParams
    Object.keys(params).forEach( key => {
      if (get(params, key)) httpParams = httpParams.set(key, get(params, key))
    })
    return httpParams
  }
}
