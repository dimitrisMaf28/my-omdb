import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { TestBed } from '@angular/core/testing'
import { first } from 'rxjs/operators'
import { RequestParams } from '../../models/request-params.interface'

import { HttpWrapperService } from './http-wrapper.service'

describe('HttpWrapperService', () => {
  let service: HttpWrapperService
  let httpTestingController: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
    })
    httpTestingController = TestBed.inject(HttpTestingController)
    service = TestBed.inject(HttpWrapperService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })


  it('should get with params', () => {
    const params: RequestParams = {
      url: 'some url', params: {s: 'value'}
    }
    const service: HttpWrapperService = TestBed.inject(HttpWrapperService)
    service.get(params).pipe(first()).subscribe(() => {})
    const res = {body: null, headers: null}
    const req = httpTestingController.expectOne(`${params.url}?s=value`)
    req.flush(res)
    expect(req.request.method).toEqual('GET')
    expect(req.request.params.get('s')).toBe('value')
  })

  it('should get without params', () => {
    const params: RequestParams = {
      url: 'some url'
    }
    const service: HttpWrapperService = TestBed.inject(HttpWrapperService)
    service.get(params).pipe(first()).subscribe(() => {})
    const res = {body: null, headers: null}
    const req = httpTestingController.expectOne(params.url)
    req.flush(res)
    expect(req.request.method).toEqual('GET')
  })

  it('should get with id', () => {
    const params: RequestParams = {
      url: 'some url', id: '12431dsf'
    }
    const service: HttpWrapperService = TestBed.inject(HttpWrapperService)
    service.get(params).pipe(first()).subscribe(() => {})
    const res = {body: null, headers: null}
    const req = httpTestingController.expectOne(`${params.url}/${params.id}`)
    req.flush(res)
    expect(req.request.method).toEqual('GET')
  })

})
