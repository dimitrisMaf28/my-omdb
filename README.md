# My OMDB



## Overview

My omdb is a barebone web app that utilizes the [omdb API](https://www.omdbapi.com/).

You can visit the app here [my-omdb.web.app](https://my-omdb.web.app/) or you can clone the repo to serve it manually.

## Manual serving

For manual serving the core library needs to be build first & then the app needs to be served.

```
ng build core-lib
ng serve -o

```

## Workspace structure

The application is build in angular workspace structure. Despite the minimal implementation, this decision was taken in order to present
a more real world case scenario. The workspace contains two projects. The first one is the core-lib library, which contains some general 
tools and modules than can be used across all workspace's projects (pagination, http wrapper & their interfaces). The second one is the 
the my-omdb application, which contains the omdb client.

## Libraries used

- Angular 12
- Bulma 
- Lodash 
- Font Awesome icons (angular version)
- Jasmine

## My Omdb structure

The app contains two views, the home page where all the titles (movies, series, episodes) can be filter with search & type and the title page where more
detailed information is presented for a selected title. Due to OMDb API constraints either the search or the id filter should be always initiated, so I 
have added a default value for the search filter. 

Both views utilize the resolver pattern in order to fetch their content based on their route details (url, params). Both resolvers use the `http wrapper` for
for fetching & error catching. The data are then passed in the component controllers via the Activated route as observables & they are consumed in the templates via the async pipe.

The two different themes are made as an scss component & they are toggled in the menu via the `data-theme` property. Furthermore, `LocalStorage` has been used to store the selected theme.

The API key is injected as a param in every request via an http interceptor & it is stored as an environment param. A future addition could be a state management solution like ngrx, to store & select loading state.
